<?php
/**
 * Secundus functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Secundus
 */

if ( ! function_exists( 'secundus_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function secundus_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Secundus, use a find and replace
		 * to change 'secundus' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'secundus', get_template_directory() . '/languages' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'secundus' ),
			'social' => esc_html__( 'Social Media Menu', 'secundus' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'gallery',
			'caption',
		) );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'secundus_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function secundus_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Contact Sidebar', 'secundus' ),
		'id'            => 'sidebar-contact',
		'description'   => esc_html__( 'Add Contact widgets here.', 'secundus' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'secundus_widgets_init' );

/**
 * Add skype to allowed protocols.
 */
function secundus_allow_skype_protocol( $protocols ){
	$protocols[] = 'skype';
	return $protocols;
}
add_filter( 'kses_allowed_protocols' , 'secundus_allow_skype_protocol' );

/**
 * Register custom fonts.
 */
function secundus_fonts_url() {
	$fonts_url = '';

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Source Sans Pro and PT Serif, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$roboto = _x( 'on', 'Roboto font: on or off', 'secundus' );
//	$pt_serif = _x( 'on', 'PT Serif font: on or off', 'secundus' );
	$font_families = array();

	if ( 'off' !== $roboto ) {
		$font_families[] = 'Roboto:300,500';
	}

	if ( in_array( 'on', array($roboto) ) ) {

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

//Custom Gallery Output
function secundus_gallery($string,$attr){

	$posts_order_string = $attr['ids'];
	$posts_order = explode(',', $posts_order_string);

	$output = "<div class=\"gallery\">";
	$posts = get_posts(array(
		'include' => $posts_order,
		'post_type' => 'attachment',
		'orderby' => 'post__in'
	));

	if($attr['orderby'] == 'rand') {
		shuffle($posts);
	}

	foreach($posts as $imagePost){
		$custom_url = get_post_meta( $imagePost->ID, '_gallery_link_url', true );
		$image = wp_get_attachment_image_src($imagePost->ID, '');
		$output .= '<figure class="gallery-item">';
		$output .= '<a target="_blank" href="'.$custom_url.'">';
		$output .= '<img src="' . $image[0] . '" width="' . $image[1] . '" height="' . $image[2] . '" alt="' . $imagePost->post_title . '" sizes="(max-width: ' . $image[1] . ') 100vw, ' . $image[1] . '" />';
		$output .= '<figcaption>';
		$output .= '<h3>' . $imagePost->post_title . '</h3>';
		$output .= $imagePost->post_excerpt;
		$output .= '</figcaption>';
		$output .= '</a>';
		$output .= '</figure>';
	}

	$output .= "</div>";
	return $output;
}
add_filter('post_gallery','secundus_gallery',10,2);


//Add SVG
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function secundus_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'secundus-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'secundus_resource_hints', 10, 2 );

function pu_remove_script_version( $src ){
	return remove_query_arg( 'ver', $src );
}

add_filter( 'script_loader_src', 'pu_remove_script_version' );
add_filter( 'style_loader_src', 'pu_remove_script_version' );

/**
 * Enqueue scripts and styles.
 */
function secundus_scripts() {

	//enqueue google fonts
	wp_enqueue_style('secundus-fonts', secundus_fonts_url() );

	wp_enqueue_style( 'secundus-style', get_stylesheet_uri() );

	wp_enqueue_script( 'footer-scripts', get_template_directory_uri() . '/js/footer-scripts.js', array(), '20151215', true );

	wp_enqueue_script( 'secundus-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_localize_script('secundus-navigation', 'secundusScreenReaderText', array(
		'expand' => __('Expand child menu', 'secundus'),
		'collapse' => __('Collapse child menu', 'secundus'),
	));
}
add_action( 'wp_enqueue_scripts', 'secundus_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Functions for the svg ions
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) )
{
	require get_template_directory() . '/inc/jetpack.php';
}