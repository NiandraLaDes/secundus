<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Secundus
 */

?>

	<footer id="colophon" class="site-footer">
		<nav id="social-navigation" class="social-navigation">
			<?php wp_nav_menu( array(
				'theme_location' => 'social',
				'menu_id'        => 'social-media-menu',
				'link_before'    => '<span class="screen-reader-text">',
				'link_after'     => '</span>' . secundus_get_svg( array( 'icon' => 'chain' ) ),
			) );
			?>
		</nav><!-- #social-navigation -->

		<?php echo '&copy; ' . date('Y') . ' <a href="https://enter-webdevelopment.nl">Enter Webdevelopment</a>'; ?>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
