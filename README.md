# Project Title

Secundus Wordpress Theme

# Description

A Wordpress theme based on the [Underscores](https://underscores.me/) theme

## Built With

* [Underscores](https://underscores.me/) - A Wordpress starters theme

## Authors

* **Bart van Enter** - *Initial work* - [Bitbucket](https://bitbucket.org/NiandraLaDes) and [Enter Webdevelopment](https://enter-webdevelopment.nl/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details